#!/usr/bin/env xonsh
import fnmatch
import io
import json
import pathlib
import shutil
import zipfile


class ModPack(zipfile.ZipFile):
    def iter_mods(self):
        for name in self.namelist():
            if not name.endswith('.jar'):
                continue
            with zipfile.ZipFile(self.open(name), 'r') as mod:
                try:
                    bdata = mod.read('mcmod.info')
                except KeyError:
                    pass
                else:
                    try:
                        modinfo = json.loads(bdata.decode('utf-8'))
                    except json.decoder.JSONDecodeError as e:
                        print(f"{self.filename}:{name}: {e}")
                    else:
                        if isinstance(modinfo, list):
                            yield from modinfo
                        elif 'modListVersion' in modinfo:
                            yield from modinfo['modList']
                        else:
                            yield modinfo

    def instance_config(self):
        for name in self.namelist():
            if not name.endswith('instance.cfg'):
                continue
            return dict(
                line.strip().split('=', 1)
                for line in io.TextIOWrapper(self.open(name))
                if line.strip()
            )

    def extract_server_packs(self, destpath):
        with zipfile.ZipFile(destpath, 'w') as dest:
            for respack in self.namelist():
                if not fnmatch.fnmatch(respack, '**/server-resource-packs/*.zip'):
                    continue
                with zipfile.ZipFile(self.open(respack)) as pack:
                    for info in pack.infolist():
                        with pack.open(info) as srcfile, dest.open(info, 'w') as destfile:
                            shutil.copyfileobj(srcfile, destfile)

        # FIXME: Handle pack.mcmeta


def render_html(packname, packdesc, mods, server_pack=''):
    body = "\n".join(f"""
<!--
{mod!r}
-->
<h2>
  <a href="{mod.get('url', '')}">{mod.get('name', '(n/a)')}</a>
  (<tt>{mod.get('modid', '')}</tt> {mod.get('version', '')})
</h2>
<p>{mod.get('description', '')}</p>
""" for mod in sorted(mods, key=lambda m: m['name']))
    return f"""<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{packname}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<h1>{packname}</h1>
<p>{packdesc}</h1>
<dl>
<dt><a href="{server_pack}">Server resource pack</a></dt>
</dl>
{body}
</body>
</html>
"""


for packfile in map(pathlib.Path, $ARGS[1:]):
    serverpack = packfile.parent / 'server-resource-packs' / packfile.name
    serverpack.parent.mkdir(exist_ok=True)
    print(f"{packfile}...")
    with ModPack(packfile) as pack:
        conf = pack.instance_config()

        pack.extract_server_packs(serverpack)

        assert packfile.suffix != '.html'
        destfile = packfile.with_suffix('.html')
        destfile.write_text(render_html(
            packname=conf['name'],
            packdesc=conf['notes'],
            server_pack=str(f'server-resource-packs/{serverpack.name}'),
            mods=pack.iter_mods(),
        ))
