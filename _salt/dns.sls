#!pyobjects


def record(host, type, value):
    BotoRoute53.present(
        "%s_%s" % (host, type),
        name=host,
        value=value,
        zone="ivyleav.es",
        record_type=type,
        ttl=(60*60),
    )


def alias(target, source, nameserver=None):
    ip4 = salt.dnsutil.A(source, nameserver=nameserver)
    if ip4:
        record(target, "A", ip4)

    ip6 = salt.dnsutil.AAAA(source, nameserver=nameserver)
    if ip6:
        record(target, "AAAA", ip6)


def getips(glob):
    ip4 = [
        ip
        for ips in salt.mine.get(glob, 'ipaddr.four').values()
        for ip in ips
    ]
    ip6 = [
        ip
        for ips in salt.mine.get(glob, 'ipaddr.six').values()
        for ip in ips
    ]
    return ip4, ip6


with BotoRoute53.hosted_zone_present(
    "hz_ivyleav.es.",
    domain_name="ivyleav.es.",
    comment="",
):
    sh4, sh6 = getips('statichost-*')
    if sh4:
        record('ivyleav.es', 'A', sh4)
        record('www.ivyleav.es', 'A', sh4)
    if sh6:
        record('ivyleav.es', 'AAAA', sh6)
        record('www.ivyleav.es', 'AAAA', sh6)

    record("ivyleav.es", "MX", [
        "10 in1-smtp.messagingengine.com.",
        "20 in2-smtp.messagingengine.com.",
    ])
    alias('mail.ivyleav.es.', 'mail.ivyleav.es.', nameserver='ns1.messagingengine.com')

    record("fm1._domainkey.ivyleav.es", "CNAME", 'fm1.ivyleav.es.dkim.fmhosted.com')
    record("fm2._domainkey.ivyleav.es", "CNAME", 'fm2.ivyleav.es.dkim.fmhosted.com')
    record("fm3._domainkey.ivyleav.es", "CNAME", 'fm3.ivyleav.es.dkim.fmhosted.com')

    record("ivyleav.es.", "TXT", [
        '"v=spf1 include:spf.messagingengine.com ~all"',
        '"keybase-site-verification=vDNHs3v0pcyOL5d9Cn_2cmCnHLQOEEAy0LksJQ3S4es"',
    ])

    # record('_minecraft._tcp.mercraft.ivyleav.es.', 'SRV', '0 5 25567 inara.doyoueven.click')
    alias('dump.ivyleav.es.', 'statichost-01.doyoueven.click')

    # House services
    # Matching static leases need to be in fred
    # TODO: Is there a more dynamic/automatic way to do this?
    record('fred.ivyleav.es', 'A', '10.6.30.1')
    record('unifi.ivyleav.es', 'A', '10.6.30.4')
    record('breadbox.ivyleav.es', 'A', '10.6.30.5')
    record('sleepy.ivyleav.es', 'A', '10.6.30.6')
    record('bakery.ivyleav.es', 'A', '10.6.30.7')
    record('transmission.ivyleav.es', 'CNAME', 'bakery.ivyleav.es.')
    record('jellyfin.ivyleav.es', 'CNAME', 'bakery.ivyleav.es.')
    record('ha.ivyleav.es', 'CNAME', 'bakery.ivyleav.es.')

    record('home.ivyleav.es', 'A', '75.12.81.89')  # First of five usable addresses.
    record('pd.ivyleav.es', 'CNAME', 'home.ivyleav.es.')


with BotoRoute53.hosted_zone_present(
    "hz_mnn.rocks.",
    domain_name="mnn.rocks.",
    comment="Mutual Nudes Network",
):
    alias('mnn.rocks', 'mulholland.doyoueven.click')
    alias('*.mnn.rocks', 'mulholland.doyoueven.click')


with BotoRoute53.hosted_zone_present(
    "hz_mnn.wiki.",
    domain_name="mnn.wiki.",
    comment="Mutual Nudes Network",
):
    record('dev.mnn.wiki', 'A', '95.216.219.206')
    record('mnn.wiki', 'A', '95.216.219.206')
    record('*.mnn.wiki', 'A', '95.216.219.206')


with BotoRoute53.hosted_zone_present(
    "hz_sphinx.rip.",
    domain_name="sphinx.rip.",
    comment="catfind",
):
    alias('sphinx.rip', 'sleepy-hamster-irvg5e9st3diudffktq9gifz.herokudns.com')
    alias('www.sphinx.rip', 'basic-llama-qbhr89p5t0567hu9wbgxvwk1.herokudns.com')
