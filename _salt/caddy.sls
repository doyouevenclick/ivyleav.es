caddy-ivyleaves:
  service.running:
    - name: caddy
    - reload: true

/srv/ivyleav.es:
  file.recurse:
    - source: salt://_source
    - clean: true

/etc/caddy/sites/ivyleav.es:
  file.managed:
    - watch_in:
      - service: caddy-ivyleaves
    - require:
      - file: /srv/ivyleav.es
    - contents: |
        ivyleav.es {
          import logging
          root * /srv/ivyleav.es
          file_server
          redir /.well-known/caldav https://carddav.fastmail.com/
          redir /.well-known/carddav https://caldav.fastmail.com/
        }

        dump.ivyleav.es {
          import logging
          root * /srv/dump.ivyleav.es
          file_server
        }

        www.ivyleav.es {
          import logging
          redir https://ivyleav.es{uri} 301
        }
